Create an environment with conda-constructor:
`conda create -n constructor constructor`

Then activate the environment:
`conda activate constructor`

Under linux run:
`constructor`

Under windows run:
`constructor --platform win-64`

Contributors:
- Francois Gallard
- Antoine Dechaume
